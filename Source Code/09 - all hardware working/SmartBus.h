// SmartBus.h
// Travis Llado
// April 11th, 2016

#include <stdint.h>

void smartbusInit(uint32_t sbFreq, uint8_t boardID, uint8_t sbPrio);
