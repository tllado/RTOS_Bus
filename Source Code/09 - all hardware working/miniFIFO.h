// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates four 64-byte FIFOs for SmartBus communications system.
// Can be read/written as bits or bytes.
// Travis Llado
// 2016.05.07

#include <stdint.h>

#define NUM_FIFOS   6
#define FIFO_LENGTH 256

uint8_t FIFOSize(uint8_t fifoNum);

uint8_t readFIFO(uint8_t fifoNum);

void writeFIFO(uint8_t fifoNum, uint8_t newByte);
