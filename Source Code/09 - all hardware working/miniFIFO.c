// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates six 256-byte FIFOs for SmartBus communications system.
// Travis Llado
// 2016.05.07

#include "miniFIFO.h"

uint8_t FIFO[NUM_FIFOS][FIFO_LENGTH] = {0};
uint8_t start[NUM_FIFOS] = {0};
uint8_t size[NUM_FIFOS] = {0};

uint8_t FIFOSize(uint8_t fifoNum) {
    return size[fifoNum];
}

uint8_t readFIFO(uint8_t fifoNum) {
    start[fifoNum]++;
    size[fifoNum]--;
    return FIFO[fifoNum][start[fifoNum] - 1];
}

void writeFIFO(uint8_t fifoNum, uint8_t newByte) {
    FIFO[fifoNum][start[fifoNum]+size[fifoNum]] = newByte;
    size[fifoNum]++;
}
