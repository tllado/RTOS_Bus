// SmartBus.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "miniFIFO.h"
#include "OS.h"
#include "Timer5.h"
#include "LEDs.h"
#include "SBPins.h"
#include "SBTimers.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t sbPriority = 7;
uint8_t sbID = 0;

void leftInTask(void) {
//	if(GPIO_PORTB_DATA_R&0x01)
//    blueLEDToggle();
//	else
		greenLEDToggle();
    OS_Kill();
}

void rightInTask(void) {
    blueLEDToggle();
    OS_Kill();
}

void leftOutTask(void) {
}

void rightOutTask(void) {
}

void tickTask(void) {
    redLEDToggle();
    if(0)
        leftOutTask();
    if(0)
        rightOutTask();
    OS_Kill();
}

void smartbusInit(uint32_t inputPeriod, uint8_t inputIdentity, uint8_t inputPriority) {
    sbPriority = inputPriority;
    sbID = inputIdentity;
    
    SBTimersInit(&tickTask, sbPriority, inputPeriod);
    SBPinsInit(&leftInTask, &rightInTask, sbPriority);
}
