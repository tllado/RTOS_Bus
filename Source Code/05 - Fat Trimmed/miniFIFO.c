// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates four 64-byte FIFOs for SmartBus communications system.
// Can be read/written as bits or bytes.
// Travis Llado
// 2016.05.07

#include "miniFIFO.h"

uint8_t FIFO[NUM_FIFOS][FIFO_LENGTH] = {0};
uint8_t writeByte[NUM_FIFOS] = {0};
uint8_t readByte[NUM_FIFOS] = {0};

uint8_t FIFONotEmpty(uint8_t fifoNum) {
    if(readByte[fifoNum]%FIFO_LENGTH == writeByte[fifoNum]%FIFO_LENGTH)
        return 0;
    return 1;
}

uint8_t FIFONotFull(uint8_t fifoNum) {
    if(readByte[fifoNum]%FIFO_LENGTH == (writeByte[fifoNum] + 1)%FIFO_LENGTH)
        return 0;
    return 1;
}

uint8_t readFIFO(uint8_t fifoNum) {
    readByte[fifoNum]++;
    return FIFO[fifoNum][(readByte[fifoNum] - 1)%FIFO_LENGTH];
}

void writeFIFO(uint8_t fifoNum, uint8_t newByte) {
    FIFO[fifoNum][(writeByte[fifoNum])%FIFO_LENGTH] = newByte;
    writeByte[fifoNum]++;
}
