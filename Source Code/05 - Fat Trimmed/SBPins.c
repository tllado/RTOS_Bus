// SBPins.h
// Travis Llado
// 2016.05.07

#include "SBPins.h"
#include "OS.h"

void(*PC6Task)(void);     // user function on falling edge of SW2(PF0)
void(*PC7Task)(void);     // user function on falling edge of SW2(PF0)
uint8_t PC6Priority = 7;
uint8_t PC7Priority = 7;

void PC6_Init(void){ 
    int32_t status = StartCritical();
    SYSCTL_RCGCGPIO_R |= 0x00000004;        // activate clock for port C   
    while((SYSCTL_PRGPIO_R&0x04) == 0) {};  // allow time for clock to stabilize
    GPIO_PORTC_DIR_R &= ~0x40;              // make PC6 in (built-in button)
    GPIO_PORTC_AFSEL_R &= ~0x40;            // disable alt funct on PC6
    GPIO_PORTC_DEN_R |= 0x40;               // enable digital I/O on PC6   
    GPIO_PORTC_PCTL_R &= ~0x0F000000;       // configure PC6 as GPIO
    GPIO_PORTC_AMSEL_R &= ~0x40;            // disable analog functionality on PC6
    GPIO_PORTC_IS_R &= ~0x40;               // PC6 is edge-sensitive
    GPIO_PORTC_IBE_R &= ~0x40;              // PC6 is not both edges
    GPIO_PORTC_IEV_R |= 0x40;               // PC6 rising edge event
    GPIO_PORTC_ICR_R = 0x40;                // clear flag6
    GPIO_PORTC_IM_R |= 0x40;                // arm interrupt on PC6
    NVIC_PRI0_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI0_R |= 0x00000000;              // priority 0
    NVIC_EN0_R = 0x00000004;                // enable interrupt 2
    EndCritical(status);
}

void PC7_Init(void){ 
    int32_t status = StartCritical();
    SYSCTL_RCGCGPIO_R |= 0x00000004;        // activate clock for port C   
    while((SYSCTL_PRGPIO_R&0x04) == 0) {};  // allow time for clock to stabilize
    GPIO_PORTC_DIR_R &= ~0x80;              // make PC7 in (built-in button)
    GPIO_PORTC_AFSEL_R &= ~0x80;            // disable alt funct on PC7
    GPIO_PORTC_DEN_R |= 0x80;               // enable digital I/O on PC7   
    GPIO_PORTC_PCTL_R &= ~0xF0000000;       // configure PC7 as GPIO
    GPIO_PORTC_AMSEL_R &= ~0x80;            // disable analog functionality on PC7
    GPIO_PORTC_IS_R &= ~0x80;               // PC7 is edge-sensitive
    GPIO_PORTC_IBE_R &= ~0x80;              // PC7 is not both edges
    GPIO_PORTC_IEV_R |= 0x80;               // PC7 rising edge event
    GPIO_PORTC_ICR_R = 0x80;                // clear flag7
    GPIO_PORTC_IM_R |= 0x80;                // arm interrupt on PC7 
    NVIC_PRI0_R &= 0xFF00FFFF;              // priority 0
    NVIC_PRI0_R |= 0x00200000;              // priority 0
    NVIC_EN0_R = 0x00000004;                // enable interrupt 2
    EndCritical(status);
}

void pinsInit(void(*task1)(void), void(*task2)(void), uint8_t prio) {
    PC6Task = task1;
    PC7Task = task2;
    PC6Priority = prio;
    PC7Priority = prio;

    PC6_Init();
    PC7_Init();
}

void GPIOPortC_Handler(void){
    if(GPIO_PORTC_RIS_R & 0x40) {   // poll PC6(BUMPER0)
        GPIO_PORTC_ICR_R = 0x40;    // acknowledge flag6
        OS_AddAperiodicThread(PC6Task, PC6Priority);
    }
    
    if(GPIO_PORTC_RIS_R & 0x80) {   // poll PC7(BUMPER1)
        GPIO_PORTC_ICR_R = 0x80;    // acknowledge flag7
        OS_AddAperiodicThread(PC7Task, PC7Priority);
    }
}
