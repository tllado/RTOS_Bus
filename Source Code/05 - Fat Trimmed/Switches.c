// Switches.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Switches.h"
#include "OS.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

void(*SW1Task)(void);     // user function on falling edge of SW1(PF4)
void(*SW2Task)(void);     // user function on falling edge of SW2(PF0)
uint8_t switchPriority = 7;

////////////////////////////////////////////////////////////////////////////////
// SW1_Init()
// Initializes Switch 1.
// Input: none
// Output: none

void SW1_Init(void){ 
	int32_t status = StartCritical();
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
	while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_DIR_R &= ~0x10;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x10;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= 0x10;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x10;  //     disable analog functionality on PF4
  GPIO_PORTF_PUR_R |= 0x10;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~0x10;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x10;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~0x10;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = 0x10;      // (e) clear flag4
  GPIO_PORTF_IM_R |= 0x10;      // (f) arm interrupt on PF4 
  NVIC_PRI7_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI7_R |= 0x00000000;    //     priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	EndCritical(status);
}

////////////////////////////////////////////////////////////////////////////////
// SW2_Init()
// Initializes Switch 2.
// Input: none
// Output: none

void SW2_Init(void){ 
	int32_t status = StartCritical();
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
	while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;    // b1) unlock GPIO Port F Commit Register
  GPIO_PORTF_CR_R |= (SW1|SW2);         // b2) enable commit for PF4 and PF0
  GPIO_PORTF_DIR_R &= ~0x01;    // (d) make PF0 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x01;  //     disable alt funct on PF0
  GPIO_PORTF_DEN_R |= 0x01;     //     enable digital I/O on PF0   
  GPIO_PORTF_PCTL_R &= ~0x0000000F; // configure PF0 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x01;  //     disable analog functionality on PF0
  GPIO_PORTF_PUR_R |= 0x01;     //     enable weak pull-up on PF0
  GPIO_PORTF_IS_R &= ~0x01;     // (e) PF0 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x01;    //     PF0 is not both edges
  GPIO_PORTF_IEV_R &= ~0x01;    //     PF0 falling edge event
  GPIO_PORTF_ICR_R = 0x01;      // (f) clear flag0
  GPIO_PORTF_IM_R |= 0x01;      // (g) arm interrupt on PF0 
  NVIC_PRI7_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI7_R |= 0x00000000;    //     priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	EndCritical(status);
}

////////////////////////////////////////////////////////////////////////////////
// switchesInit()
// Initialized hardware and software associated with Launchpad's two onboard
//  switches
// Input: none
// Output: none

void switchesInit(void(*task1)(void), void(*task2)(void), uint8_t swPrio) {
	SW1Task = task1;
	SW2Task = task2;
	switchPriority = swPrio;
	
	SW1_Init();
	SW2_Init();
}

////////////////////////////////////////////////////////////////////////////////
// GPIOPortF_Handler()
// Decides what action to take for any interrupt on Port F. This includes the
//  Launchpad's two onboard switches.
// Input: none
// Output: none

void GPIOPortF_Handler(void){
	if(GPIO_PORTF_RIS_R & 0x10){  // poll PF4(SW1)
    GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
    OS_AddAperiodicThread(SW1Task, switchPriority);
	}
	if(GPIO_PORTF_RIS_R & 0x01){  // poll PF0(SW2)
    GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
    OS_AddAperiodicThread(SW2Task, switchPriority);
	}
}
