// SmartBus.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "miniFIFO.h"
#include "OS.h"
#include "Timer5.h"
#include "LEDs.h"
#include "SBPins.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t sbPriority = 7;
uint8_t sbID = 0;

void PC6High(void) {
    greenLEDToggle();
    OS_Kill();
}

void PC7High(void) {
    blueLEDToggle();
    OS_Kill();
}

void tickTask(void) {
	redLEDToggle();
}

void smartbusInit(uint32_t sbPeriod, uint8_t sbIdent, uint8_t sbPrio) {
    sbPriority = sbPrio;
    sbID = sbIdent;
    Timer5_Init(&tickTask, sbPriority, sbPeriod);
    pinsInit(&PC6High, &PC7High, sbPriority);
}
