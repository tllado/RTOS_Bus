// SBPins.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

void pinsInit(void(*task1)(void), void(*task2)(void), uint8_t prio);
