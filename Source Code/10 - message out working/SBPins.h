// SBPins.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

/////
// Global Variables

#define rDatIn	(*((volatile uint32_t *)0x40005004))	// PB0
#define lDatIn	(*((volatile uint32_t *)0x40005008))	// PB1
//#define PB2 (*((volatile uint32_t *)0x40005010))	// PB2, not in use
//#define PB3 (*((volatile uint32_t *)0x40005020))	// PB3, not in use
#define rClkOut (*((volatile uint32_t *)0x40005040))	// PB4
#define rDatOut (*((volatile uint32_t *)0x40005080))	// PB5
#define lClkOut (*((volatile uint32_t *)0x40005100))	// PB6
#define lDatOut (*((volatile uint32_t *)0x40005200))	// PB7


////////////////////////////////////////////////////////////////////////////////
// Prototypes

void SBPinsInit(void(*inputTask1)(void), void(*inputTask2)(void), uint8_t inputPriority);
