// SmartBus.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "miniFIFO.h"
#include "OS.h"
#include "Timer5.h"
#include "LEDs.h"
#include "SBPins.h"
#include "SBTimers.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t sbPriority = 7;
uint8_t sbID = 0;
uint8_t upTick = 0;
uint8_t leftOutByte = 0;
uint8_t leftOutCount = 0;
uint8_t rightOutByte = 0;
uint8_t rightOutCount = 0;
uint8_t leftInByte = 0;
uint8_t leftInCount = 0;
uint8_t rightInByte = 0;
uint8_t rightInCount = 0;

void bitInLeft(void) {
    OS_Kill();
}

void bitInRight(void) {
    OS_Kill();
}

void tickTask(void) {
    upTick ^= 1;
    
    if(upTick) {
        redLEDOn();
				
				if(leftOutCount > 0) {
					lClkOut = 0xFF;
//					blueLEDOn();
				}
				if(rightOutCount > 0) {
					rClkOut = 0xFF;
//					greenLEDOn();
				}
		}
		else {
        redLEDOff();
			
			  rClkOut = 0x00;
			greenLEDOff();
			  lClkOut = 0x00;
			blueLEDOff();
			  rDatOut = 0x00;
			  lDatOut = 0x00;
			  
			  if(leftOutCount > 0) {
						leftOutCount--;
					  uint8_t nextBit = (leftOutByte&(1<<leftOutCount))>>leftOutCount;
						lDatOut = 0xFF*nextBit;
						if(nextBit)
							greenLEDOn();
						else
							greenLEDOff();
				}
				if(leftOutCount == 0 && FIFOSize(left) > 0) {
						leftOutCount = 8;
						leftOutByte = FIFORead(left);
				}
				
				if(rightOutCount > 0) {
						rightOutCount--;
					  uint8_t nextBit = (rightOutByte&(1<<rightOutCount))>>rightOutCount;
						rDatOut = 0xFF*nextBit;
						if(nextBit)
							blueLEDOn();
						else
							blueLEDOff();
				}
				if(rightOutCount == 0 && FIFOSize(right) > 0) {
						rightOutCount = 8;
						rightOutByte = FIFORead(right);
				}
		}
    
    OS_Kill();
}

void SBInit(uint32_t inputPeriod, uint8_t inputIdentity, uint8_t inputPriority) {
    sbPriority = inputPriority;
    sbID = inputIdentity;
    
    SBTimersInit(&tickTask, sbPriority, inputPeriod);
    SBPinsInit(&bitInLeft, &bitInRight, sbPriority);
}
