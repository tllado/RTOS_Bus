// RTCommsProject.c
// Runs on TM4C123
// Real time communications program requiring only GPIO pins
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "Config.h"
#include "Interrupts.h"
#include "RTCommsProject.h"
#include "OS.h"
#include "SysTick.h"
#include "Timer5.h"
#include "tm4c123gh6pm.h"

void IdleTask(void){ 
    while(1) { 
//        PF1 ^= 0x02;
        WaitForInterrupt();
    }
}

void SW1Press(void){
}

void SW2Press(void) {
}

void PC6High(void) {
	PF2 ^= 0x04;
}

void PC7High(void) {
	PF3 ^= 0x08;
}

int main(void){
    OS_Init();
    PortF_Init();
		Timer5_Init();
//    SW1_Init(&SW1Press,3);
//    SW2_Init(&SW2Press,3);
    PC6_Init(&PC6High,3);
    PC7_Init(&PC7High,3);
    OS_AddThread(&IdleTask, 128, 7);
    OS_Launch(TIMESLICE);
    return 0;
}
