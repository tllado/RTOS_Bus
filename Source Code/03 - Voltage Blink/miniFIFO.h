// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates four 64-byte FIFOs for SmartBus communications system.
// Can be read/written as bits or bytes.
// Travis Llado
// 2016.05.07

#include <stdint.h>
#include "Config.h"

uint8_t FIFONotEmpty(uint8_t);

uint8_t FIFONotFull(uint8_t);

uint8_t readFIFO(uint8_t);

void writeFIFO(uint8_t, uint8_t);
