// SmartBus.h
// Travis Llado
// April 11th, 2016

#include <stdint.h>

#define SW1       0x10                      // on the left side of the Launchpad board
#define SW2       0x01                      // on the right side of the Launchpad board
#define PF1  (*((volatile unsigned long *)0x40025008))
#define PF2  (*((volatile unsigned long *)0x40025010))
#define PF3  (*((volatile unsigned long *)0x40025020))

extern volatile uint8_t PC6_Data;    // Bumper0 data
extern volatile uint8_t PC7_Data;    // Bumper1 data

void PortF_Init(void);
void SW1_Init(void(*task)(void), uint8_t priority);
void SW2_Init(void(*task)(void), uint8_t priority);
void PC6_Init(void(*task)(void), uint8_t priority);
void PC7_Init(void(*task)(void), uint8_t priority);
void Switch_Enable(void);
void Switch_Disable(void);
void PC6Task(void);
void PC7Task(void);
