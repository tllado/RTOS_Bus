// Main.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include "OS.h"
#include "SmartBus.h"
#include "LEDs.h"
#include "Switches.h"
#include "SBPins.h"

////////////////////////////////////////////////////////////////////////////////
// idleTask()
// Doesn't do much of anything.
// Input: none
// Output: none

void IdleTask(void){ 
    while(1) { 
        WaitForInterrupt();
    }
}

////////////////////////////////////////////////////////////////////////////////
// leftSwitch()
// Action performed when left switch is pressed.
// Input: none
// Output: none

void leftSwitch(void) {
	SBWrite(0xFF);
	SBWrite(0xFF);
	SBWrite(0xFF);
	SBWrite(0xFF);
}

////////////////////////////////////////////////////////////////////////////////
// rightSwitch()
// Action performed when right switch is pressed.
// Input: none
// Output: none

void rightSwitch(void) {
	SBWrite(0xAA);
	SBWrite(0xAA);
	SBWrite(0xAA);
	SBWrite(0xAA);
}

////////////////////////////////////////////////////////////////////////////////
// main()
// Initializes OS and all programs
// Input: none
// Output: none

int main(void){
    OS_Init();
    LEDsInit();
    switchesInit(&leftSwitch, &rightSwitch, SWITCH_PRIO);
    SBInit(SYS_CLK_FREQ/SBUS_FREQ, SBUS_ID, SBUS_PRIO);
    OS_AddThread(&IdleTask, 128, 7);
    OS_Launch(SYS_CLK_FREQ/OS_FREQ);
    return 0;
}
