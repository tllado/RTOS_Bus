// Main.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include "OS.h"
#include "SmartBus.h"
#include "LEDs.h"
#include "Switches.h"

////////////////////////////////////////////////////////////////////////////////
// idleTask()
// Doesn't do much of anything.
// Input: none
// Output: none

void IdleTask(void){ 
    while(1) { 
        WaitForInterrupt();
    }
}

////////////////////////////////////////////////////////////////////////////////
// leftSwitch()
// Action performed when left switch is pressed.
// Input: none
// Output: none

void leftSwitch(void) {
    greenLEDToggle();
}

////////////////////////////////////////////////////////////////////////////////
// rightSwitch()
// Action performed when right switch is pressed.
// Input: none
// Output: none

void rightSwitch(void) {
    blueLEDToggle();
}

//void portBInit(void) {
//	    DisableInterrupts();
//        SYSCTL_RCGCGPIO_R |= 0x00000002;   // activate clock for Port B
//        while((SYSCTL_PRGPIO_R&0x02) == 0) {}
//                                        // allow time for clock to start
//        GPIO_PORTB_AMSEL_R = 0x00;      // disable analog on PB0-7
//        GPIO_PORTB_PCTL_R = 0x00000000; // PCTL GPIO on PB0-7
//        GPIO_PORTB_DIR_R = 0x33;        // PB0/1/4/5 input, PB2/3/6/7 output
//        GPIO_PORTB_AFSEL_R = 0x00;      // disable alt funct on PB0-7
//        GPIO_PORTB_DEN_R = 0xFF;        // enable digital I/O on PB0-7

//        GPIO_PORTB_IS_R = 0x00;         // set PB pins to be edge sensitive
//        GPIO_PORTB_IBE_R = 0x00;        // set PB pins to be single edge
//        GPIO_PORTB_IEV_R = 0xFF;        // set PB pins to rising edge
//        GPIO_PORTB_ICR_R = 0xFF;        // clear PB interrupts
//        GPIO_PORTB_IM_R |= 0x22;        // arm PB1/5 interrupts

//        NVIC_PRI0_R &= 0xFF00FFFF;      // priority 0
//        NVIC_PRI0_R |= 0x00000000;      // priority 0
//        NVIC_EN0_R = 0x00000002;        // enable interrupt 1
//    EnableInterrupts();
//}

////////////////////////////////////////////////////////////////////////////////
// main()
// Initializes OS and all programs
// Input: none
// Output: none

int main(void){
    OS_Init();
    LEDsInit();
//		portBInit();
    switchesInit(&leftSwitch, &rightSwitch, SWITCH_PRIO);
    smartbusInit(SYS_CLK_FREQ/SBUS_FREQ, SBUS_ID, SBUS_PRIO);
    OS_AddThread(&IdleTask, 128, 7);
    OS_Launch(SYS_CLK_FREQ/OS_FREQ);
    return 0;
}
