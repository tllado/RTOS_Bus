// Timer5.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Timer5A_Handler
// Initialized Timer 5A for use by SmartBus.c.
// Input: task to perform each tick, task priority, and timer period
// Output: none

void Timer5_Init(void(*T5Task)(void), uint8_t T5Prio, uint32_t period);
