// Switches.h
// Travis Llado
// 2016.05.07

#include <stdint.h>

#define SW1		0x10                      // on the left side of the Launchpad board
#define SW2 	0x01                      // on the right side of the Launchpad board

void switchesInit(void(*task1)(void), void(*task2)(void), uint8_t swPrio);
