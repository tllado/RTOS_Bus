// Config.h
// Runs on TM4C123 
// Create and manage threads.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Program.h
#define BOARD_ID    1

// Timer5.h
#define ONE_MS          80000
#define CLOCK_PERIOD    ONE_MS*500

// miniFIFO.h
#define NUM_FIFOS   6
#define FIFO_LENGTH 64
