// Ping.h
// Runs on TM4C123
// uses ping PB2, PB4, PB6, PC5, for falling edge triggered interrupts 
// to capture the pulse width for sonar Ping))) sensors.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


//********************Constants*********************************************  
#define PB4                     (*((volatile unsigned long *)0x40005040))

#define NVIC_EN0_INT21          1<<21       // Interrupt 21 enable
#define TIMER_TAILR_TAILRL_M    0x0000FFFF  // GPTM TimerA Interval Load Register Low
#define SIZE_OF_LOOKUP          76
#define TEMP_CELSIUS            25          // current temperature in celcius
#define C_AIR                   (3315+(6*TEMP_CELSIUS))/1000   // speed of air in sound (cm/.1ms)  
//**************************************************************************


//********************Macros************************************************ 
#define DIST(x) ((C_AIR * x)>>1)	          // derive distance from pulsewidth
//**************************************************************************


//********************Extern************************************************ 
extern volatile uint32_t Ping1_Dist;          // calibrated ping1 sensor data
//**************************************************************************


//********************Prototypes********************************************


//******** Ping_Init *************** 
// Initializes ping timer for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// inputs:  none
// outputs: none
void Ping_Init(void);

//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  none
// outputs: none
void SendPulse(void);

//******** ConfigForCapture *************** 
// Configure pin for input capture using a pull up resistor
// Called from Ping#().
// inputs:  none
// outputs: none
void ConfigForCapture(void);

//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void);

//******** PingCompute *************** 
// Periodic background thread.
// Translated measured ping values to actual 
// values using a lookup table
// inputs:  none
// outputs: none
void PingCompute(void);

//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void);
