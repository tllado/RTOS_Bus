// Switches.h
// Travis Llado
// 2016.05.07

#define SW1_PRIORITY	3      // priority of SW1's user task
#define SW2_PRIORITY	3      // priority of SW2's user task
#define SW1		0x10                      // on the left side of the Launchpad board
#define SW2 	0x01                      // on the right side of the Launchpad board

void switchesInit(void);
