// LEDs.c
// Travis Llado
// 2016.05.07

#include "LEDs.h"

void LEDsInit(void) {
		int32_t status = StartCritical();
		SYSCTL_RCGCGPIO_R |= 0x20;            // activate port F
    while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
    GPIO_PORTF_DIR_R |= 0x0E;             // make PF3-1 output (PF3-1 built-in LEDs)
    GPIO_PORTF_AFSEL_R &= ~0x0E;          // disable alt funct on PF3-1
    GPIO_PORTF_DEN_R |= 0x0E;             // enable digital I/O on PF3-1
    GPIO_PORTF_PCTL_R &= ~0x0000FFF0;     // configure PF3-1 as GPIO
    GPIO_PORTF_AMSEL_R &= ~0x0E;          // disable analog functionality on PF3-1
		EndCritical(status);
}

void RedLEDOn(void) {
	PF1 = 0x02;
}

void RedLEDOff(void) {
	PF1 &= ~0x02;
}

void RedLEDToggle(void) {
	PF1 ^= 0x02;
}

void GreenLEDOn(void) {
	PF3 = 0x08;
}

void GreenLEDOff(void) {
	PF3 &= ~0x08;
}

void GreenLEDToggle(void) {
	PF3 ^= 0x08;
}

void BlueLEDOn(void) {
	PF2 = 0x04;
}

void BlueLEDOff(void) {
	PF2 &= ~0x04;
}

void BlueLEDToggle(void) {
	PF2 ^= 0x04;
}
