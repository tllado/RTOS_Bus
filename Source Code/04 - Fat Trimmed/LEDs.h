// LEDs.h
// Travis Llado
// 2016.05.07

#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Interrupts.h"

#define PF1	(*((volatile unsigned long *)0x40025008))
#define PF2 (*((volatile unsigned long *)0x40025010))
#define PF3 (*((volatile unsigned long *)0x40025020))

void LEDsInit(void);
void RedLEDOn(void);
void RedLEDOff(void);
void RedLEDToggle(void);
void GreenLEDOn(void);
void GreenLEDOff(void);
void GreenLEDToggle(void);
void BlueLEDOn(void);
void BlueLEDOff(void);
void BlueLEDToggle(void);
