// SmartBus.h
// Travis Llado
// April 11th, 2016

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// System Settings
#define PC6_PRIORITY	3  // priority of BUMPER0's user task
#define PC7_PRIORITY	3  // priority of BUMPER1's user task

void smartbusInit(void);
