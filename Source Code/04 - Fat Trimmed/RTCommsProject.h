// RTCommsProject.h
// Runs on TM4C123
// 
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "Config.h"
