// RTCommsProject.c
// Runs on TM4C123
// Real time communications program requiring only GPIO pins
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "Config.h"
#include "Interrupts.h"
#include "RTCommsProject.h"
#include "OS.h"
#include "SysTick.h"
#include "Timer5.h"
#include "tm4c123gh6pm.h"
#include "LEDs.h"
#include "Switches.h"

void IdleTask(void){ 
    while(1) { 
//        PF1 ^= 0x02;
        WaitForInterrupt();
    }
}

int main(void){
    OS_Init();
    LEDsInit();
		switchesInit();
    smartbusInit();
    OS_AddThread(&IdleTask, 128, 7);
    OS_Launch(TIMESLICE);
    return 0;
}
