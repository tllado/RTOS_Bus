// SBPins.h
// Travis Llado
// 2016.05.07

#include "SBPins.h"
#include "OS.h"
#include "LEDs.h"

void(*lInTask)(void);     // user function on falling edge of SW2(PF0)
void(*rInTask)(void);     // user function on falling edge of SW2(PF0)
uint8_t PBPriority = 7;

void PCInit(void) { 
    int32_t status = StartCritical();
        SYSCTL_RCGCGPIO_R |= 0x04; // (a) activate clock for port C   
        while((SYSCTL_PRGPIO_R&0x04) == 0) {} // allow time for clock to stabilize
        GPIO_PORTC_DIR_R &= ~0xC0;    // (d) make PC6 in (built-in button)
        GPIO_PORTC_AFSEL_R &= ~0xC0;  //     disable alt funct on PC6
        GPIO_PORTC_DEN_R |= 0xC0;     //     enable digital I/O on PC6   
        GPIO_PORTC_PCTL_R &= ~0xFF000000; // configure PC6 as GPIO
        GPIO_PORTC_AMSEL_R &= ~0xC0;  //     disable analog functionality on PC6
        GPIO_PORTC_PUR_R &= ~0xC0;        // disable all pulldown resistors
        GPIO_PORTC_PDR_R |= 0xC0;        // enable all pulldown resistors

        GPIO_PORTC_IS_R &= ~0xC0;     // (e) PC6 is edge-sensitive
        GPIO_PORTC_IBE_R &= ~0xC0;    //     PC6 is not both edges
        GPIO_PORTC_IEV_R |= 0xC0;     //     PC6 rising edge event
        GPIO_PORTC_ICR_R = 0xC0;      // (f) clear flag6
        GPIO_PORTC_IM_R |= 0xC0;      // (g) arm interrupt on PC6

        NVIC_PRI0_R &= 0xFF00FFFF;    //     priority 0
        NVIC_PRI0_R |= 0x00200000;    //     priority 0
        NVIC_EN0_R = 0x04;      // enable interrupt 2
    EndCritical(status);
}

void portBInit(void) {
    int32_t status = StartCritical();
        SYSCTL_RCGCGPIO_R |= 0x02;        // 1) activate clock for Port B
  while((SYSCTL_PRGPIO_R&0x02) == 0){};// ready?
  GPIO_PORTB_DIR_R &= ~0xFF;        // PB1 is an input
  GPIO_PORTB_AFSEL_R &= ~0xFF;      // regular port function
  GPIO_PORTB_AMSEL_R &= ~0xFF;      // disable analog on PB1 
  GPIO_PORTB_PCTL_R &= ~0xFFFFFFFF; // PCTL GPIO on PB1 
  GPIO_PORTB_DEN_R |= 0xFF;         // PB3-0 enabled as a digital port
    EndCritical(status);
}

void SBPinsInit(void(*inputTask1)(void), void(*inputTask2)(void), uint8_t inputPriority) {
    lInTask = inputTask1;
    rInTask = inputTask2;
    PBPriority = inputPriority;
    
    PCInit();
    portBInit();
}

void GPIOPortB_Handler(void){
    if(GPIO_PORTB_RIS_R & 0x02) { // poll PB1
    GPIO_PORTB_ICR_R = 0x02;    // acknowledge flag1
    OS_AddAperiodicThread(lInTask, PBPriority); 
    }

    if(GPIO_PORTB_RIS_R & 0x20) { // poll PB5
    GPIO_PORTB_ICR_R = 0x20;    // acknowledge flag5
    OS_AddAperiodicThread(rInTask, PBPriority);
    }
}

void GPIOPortC_Handler(void){
    if(GPIO_PORTC_RIS_R & 0x40) { // poll PC6(BUMPER0)
    GPIO_PORTC_ICR_R = 0x40;    // acknowledge flag6
    OS_AddAperiodicThread(lInTask, PBPriority); 
    }

    if(GPIO_PORTC_RIS_R & 0x80) { // poll PC7(BUMPER1)
    GPIO_PORTC_ICR_R = 0x80;    // acknowledge flag7
    OS_AddAperiodicThread(rInTask, PBPriority);
    }
}
