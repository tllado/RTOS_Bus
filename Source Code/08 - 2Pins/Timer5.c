// Timer5.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Timer5.h"
#include "OS.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

void(*Timer5Task)(void);     // user function on falling edge of SW2(PF0)
uint8_t Timer5Priority = 7;

////////////////////////////////////////////////////////////////////////////////
// Timer5A_Handler
// Initialized Timer 5A for use by SmartBus.c.
// Input: period of Timer 5A
// Output: none

void Timer5_Init(void(*T5Task)(void), uint8_t T5Prio, uint32_t period) {
    Timer5Task = T5Task;
    Timer5Priority = T5Prio;

    SYSCTL_RCGCTIMER_R |= 0x20;             // activate timer5
    while((SYSCTL_PRTIMER_R&0x20) == 0) {}  // allow time for clock to stabilize
    TIMER5_CTL_R = 0x00000000;              // disable timer5A during setup
    TIMER5_CFG_R = 0x00000000;              // configure for 32-bit mode
    TIMER5_TAMR_R = 0x00000002;             // configure for periodic mode
    TIMER5_TAILR_R = period - 1;            // reload value
    TIMER5_TAPR_R = 0;                      // bus clock resolution
    TIMER5_ICR_R = 0x00000001;              // clear timer5A timeout flag
    TIMER5_IMR_R = 0x00000001;              // arm timeout interrupt
    NVIC_PRI23_R &=   0xFFFFFF00;           // priority 0
    NVIC_PRI23_R |=   0x00000000;           // priority 0
    NVIC_EN2_R = 1<<(92-(32*2));            // enable IRQ 92 in NVIC
    TIMER5_CTL_R = 0x00000001;              // enable timer5A
}

////////////////////////////////////////////////////////////////////////////////
// Timer5A_Handler
// Performs action when Timer 5A interrupts.
// Input: none
// Output: none

void Timer5A_Handler(void) {
    DisableInterrupts();
    TIMER5_ICR_R = TIMER_ICR_TATOCINT;  // acknowledge TIMER5A timeout
    OS_AddAperiodicThread(Timer5Task, Timer5Priority);
    EnableInterrupts();
}
