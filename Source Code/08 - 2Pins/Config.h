// Config.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define SYS_CLK_FREQ    80000000    // Hz
#define OS_FREQ         1000        // Hz
#define SBUS_FREQ       2           // Hz
#define SBUS_ID         0x01
#define SBUS_PRIO       2
#define SWITCH_PRIO     3
