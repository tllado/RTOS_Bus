// SBTimers.c
// Travis Llado
// 2016.05.08

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SBTimers.h"
#include "OS.h"
#include "LEDs.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

void(*timer1Task)(void);   // smartbus function for Timer1A tick
uint8_t timer1Priority = 7;

////////////////////////////////////////////////////////////////////////////////
// timer1Enable()

void timer1Enable(void) {
    TIMER1_CTL_R = 0x00000001;  // enable Timer 1A
}

////////////////////////////////////////////////////////////////////////////////
// timer1Disable()

void timer1Disable(void) {
    TIMER1_CTL_R = 0x00000000;  // disable Timer 1A
}

////////////////////////////////////////////////////////////////////////////////
// timer1Init()

int timer1Init(uint32_t period, uint8_t priority) {
    DisableInterrupts();
        SYSCTL_RCGCTIMER_R |= 0x02;         // activate Timer1
        while(SYSCTL_RCGCTIMER_R == 0);     // allow time for timer to start
        TIMER1_CTL_R = 0x00000000;          // disable Timer1A during setup
        TIMER1_CFG_R = 0x00000000;          // set to 32-bit mode
        TIMER1_TAMR_R = 0x00000002;         // set to periodic mode
        TIMER1_TAILR_R = period - 1;   // set reset value
        TIMER1_TAPR_R = 0;                  // set bus clock resolution
        TIMER1_ICR_R = 0x00000001;          // clear Timer1A timeout flag
        TIMER1_IMR_R |= 0x00000001;         // arm timeout interrupt
        NVIC_PRI5_R = (NVIC_PRI5_R & 0x00FFFFFF) | (priority << 29);
//                                            // set priority
        NVIC_EN0_R = 1 << 21;               // enable IRQ 21 in NVIC

        timer1Enable();
    EnableInterrupts();
  return 1;
}

////////////////////////////////////////////////////////////////////////////////
// timer1A_Handler

void Timer1A_Handler(void) {
    TIMER1_ICR_R = 0x00000001;      // acknowledge timer1A timeout
//		GPIO_PORTB_DATA_R ^= 0xFF;
    OS_AddAperiodicThread(timer1Task, timer1Priority);
}

////////////////////////////////////////////////////////////////////////////////
// SBTimersInit()

void SBTimersInit(void(*inputTask)(void), uint8_t inputPriority, uint32_t inputPeriod) {
    timer1Task = inputTask;
    timer1Priority = inputPriority;

    timer1Init(inputPeriod/2, timer1Priority);
}
