// Main.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include "OS.h"
#include "SmartBus.h"
#include "LEDs.h"
#include "Switches.h"
#include "SBPins.h"

////////////////////////////////////////////////////////////////////////////////
// idleTask()
// Doesn't do much of anything.
// Input: none
// Output: none

void IdleTask(void){ 
    while(1) { 
        WaitForInterrupt();
    }
}

////////////////////////////////////////////////////////////////////////////////
// leftSwitch()
// Action performed when left switch is pressed.
// Input: none
// Output: none

void leftSwitch(void) {
//    SBWrite(LEFT_MESSAGE);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
}

////////////////////////////////////////////////////////////////////////////////
// rightSwitch()
// Action performed when right switch is pressed.
// Input: none
// Output: none

void rightSwitch(void) {
//    SBWrite(RIGHT_MESSAGE);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
    SBWrite(0xAA);
}

////////////////////////////////////////////////////////////////////////////////
// updateLEDs()
// Checks for messages. If have messages, updates LEDs.
// input: none
// output: none

void updateLEDs(void) {
    if(SBAvailable() > 0) {
        int throwaway = SBRead();
        redLEDToggle();
    }
}

////////////////////////////////////////////////////////////////////////////////
// main()
// Initializes OS and all programs
// Input: none
// Output: none

int main(void){
    OS_Init();
    LEDsInit();
    switchesInit(&leftSwitch, &rightSwitch, SWITCH_PRIO);
    SBInit(SYS_CLK_FREQ/SBUS_FREQ, SBUS_ID, SBUS_PRIO);
    OS_AddThread(&IdleTask, 128, 7);
//    OS_AddPeriodicThread(&updateLEDs, SYS_CLK_FREQ/10, 3);
    OS_Launch(SYS_CLK_FREQ/OS_FREQ);
    return 0;
}
