// LEDs.h
// Travis Llado
// 2016.05.07

#define PF1	(*((volatile unsigned long *)0x40025008))
#define PF2 (*((volatile unsigned long *)0x40025010))
#define PF3 (*((volatile unsigned long *)0x40025020))

void LEDsInit(void);
void redLEDOn(void);
void redLEDOff(void);
void redLEDToggle(void);
void greenLEDOn(void);
void greenLEDOff(void);
void greenLEDToggle(void);
void blueLEDOn(void);
void blueLEDOff(void);
void blueLEDToggle(void);
