// SBTimers.h
// Travis Llado
// 2016.05.08

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// SBTimersInit()
// Runs Timer1 initialization functions and assigned interrupt task
// input: pointer to interrupt task, interrupt task priority
// output: none

void SBTimersInit(void(*inputTask)(void), uint8_t inputPriority, uint32_t inputPeriod);
