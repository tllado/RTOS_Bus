// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates four 64-byte FIFOs for SmartBus communications system.
// Can be read/written as bits or bytes.
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define NUM_FIFOS   3
#define FIFO_LENGTH 256

////////////////////////////////////////////////////////////////////////////////
// FIFOSize()
// Returns current size of data stored in FIFO
// input: FIFO number to be measured
// output: size of FIFO

uint8_t FIFOSize(uint8_t fifoNum);

////////////////////////////////////////////////////////////////////////////////
// FIFORead()
// Returns single byte from FIFO
// input: number of FIFO to be read from
// output: single byte from specified FIFO

uint8_t FIFORead(uint8_t fifoNum);

////////////////////////////////////////////////////////////////////////////////
// FIFOWrite
// Writes single byte into specified FIFO
// input: number of FIFO to be written to, byte to be written
// output: none

void FIFOWrite(uint8_t fifoNum, uint8_t newByte);
