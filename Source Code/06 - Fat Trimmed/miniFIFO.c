// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates six 256-byte FIFOs for SmartBus communications system.
// Travis Llado
// 2016.05.07

#include "miniFIFO.h"

uint8_t FIFO[NUM_FIFOS][FIFO_LENGTH] = {0};
uint8_t writeByte[NUM_FIFOS] = {0};
uint8_t readByte[NUM_FIFOS] = {0};

uint8_t FIFONotEmpty(uint8_t fifoNum) {
    if(readByte[fifoNum] == writeByte[fifoNum])
        return 0;
    return 1;
}

uint8_t FIFONotFull(uint8_t fifoNum) {
    if(readByte[fifoNum] == (writeByte[fifoNum] + 1))
        return 0;
    return 1;
}

uint8_t readFIFO(uint8_t fifoNum) {
    readByte[fifoNum]++;
    return FIFO[fifoNum][readByte[fifoNum] - 1];
}

void writeFIFO(uint8_t fifoNum, uint8_t newByte) {
    FIFO[fifoNum][writeByte[fifoNum]] = newByte;
    writeByte[fifoNum]++;
}
