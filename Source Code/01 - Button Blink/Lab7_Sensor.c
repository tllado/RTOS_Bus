// RTCom.c
// Runs on TM4C123
// Real time communications program requiring only GPIO pins
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "Buttons.h"
#include "Config.h"
#include "Interrupts.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "Profiler.h"
#include "Retarget.h"
#include "SysTick.h"
#include "Timer5.h"
#include "tm4c123gh6pm.h"

uint8_t BlueBlink = 0;
uint8_t GreenBlink = 0;

void IdleTask(void){ 
    while(1) { 
        PF1 ^= 0x02;
        WaitForInterrupt();
    }
}

void SW1Press(void){
    BlueBlink ^= 1;
}

void SW2Press(void) {
    GreenBlink ^= 1;
}

int main(void){
    OS_Init();
    PortF_Init();
		Timer5_Init();
    OS_AddButtonTask(&SW1Press, SW1_TASK, 3);
    OS_AddButtonTask(&SW2Press, SW2_TASK, 3);
    OS_AddThread(&IdleTask, 128, 7);
    OS_Launch(TIMESLICE);
    return 0;
}
