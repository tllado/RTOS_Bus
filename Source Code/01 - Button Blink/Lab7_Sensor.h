// Lab7_Sensor.h
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Global Variables

extern uint8_t PingDebounce;	// only add one thread when button is pressed
extern uint8_t BlueBlink;
extern uint8_t GreenBlink;
