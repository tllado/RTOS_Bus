// Config.h
// User Settings for implementation of SmartBus realtime communications protocol
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define SYS_CLK_FREQ    80000000    // Hz
#define OS_FREQ         500         // Hz
#define SBUS_FREQ       50          // Hz
#define SBUS_ID         0x30
#define LEFT_MESSAGE    0x1F
#define RIGHT_MESSAGE   0x2F
#define SBUS_PRIO       1
#define SWITCH_PRIO     3
