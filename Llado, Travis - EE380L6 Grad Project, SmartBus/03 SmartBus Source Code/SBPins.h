// SBPins.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define lDatIn  (*((volatile uint32_t *)0x40005004))    // PB0
#define rDatIn  (*((volatile uint32_t *)0x40005008))    // PB1
//#define PB2 (*((volatile uint32_t *)0x40005010))  // PB2, not in use
//#define PB3 (*((volatile uint32_t *)0x40005020))  // PB3, not in use
#define lClkOut (*((volatile uint32_t *)0x40005040))    // PB4
#define rClkOut (*((volatile uint32_t *)0x40005080))    // PB5
#define lDatOut (*((volatile uint32_t *)0x40005100))    // PB6
#define rDatOut (*((volatile uint32_t *)0x40005200))    // PB7

////////////////////////////////////////////////////////////////////////////////
// SBPinsInit()
// Initializes GPIO pins and assigns interrupt tasks for all pins used by
//  SmartBus program.
// input: pointers to two interrupt action tasks and priority for said tasks
// output: none

void SBPinsInit(void(*inputTask1)(void), void(*inputTask2)(void),              \
    uint8_t inputPriority);
