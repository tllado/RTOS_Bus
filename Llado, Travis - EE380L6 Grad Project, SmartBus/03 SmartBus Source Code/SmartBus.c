// SmartBus.c
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "SmartBus.h"
#include "miniFIFO.h"
#include "OS.h"
#include "LEDs.h"
#include "SBPins.h"
#include "SBTimers.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t sbPriority = 7;
uint8_t sbID = 0;
uint8_t upTick = 0;
uint8_t leftOutByte = 0;
uint8_t leftOutCount = 0;
uint8_t rightOutByte = 0;
uint8_t rightOutCount = 0;
uint8_t leftInByte = 0;
uint8_t leftInCount = 0;
uint8_t rightInByte = 0;
uint8_t rightInCount = 0;

////////////////////////////////////////////////////////////////////////////////
// leftInTick()
// Action taken when left read clock ticks. Read single bit. If byte is
//  complete, writes bytes to FIFOs and starts on new byte.
// input: none
// output: none

void leftInTick(void) {
    uint8_t newBit = lDatIn;
    leftInByte += newBit << leftInCount;
    leftInCount++;
    
    if(leftInCount == 8) {
        int32_t status = StartCritical();
            FIFOWrite(right, leftInByte);
            if((leftInByte&ID_MASK) == sbID)
                FIFOWrite(up, leftInByte);
        EndCritical(status);
        leftInCount = 0;
    }

    OS_Kill();
}

////////////////////////////////////////////////////////////////////////////////
// rightInTick()
// Action taken when right read clock ticks. Read single bit. If byte is
//  complete, writes bytes to FIFOs and starts on new byte.
// input: none
// output: none

void rightInTick(void) {
    uint8_t newBit = rDatIn >> 1;
    rightInByte += newBit << rightInCount;
    rightInCount++;
    
    if(rightInCount == 8) {
        int32_t status = StartCritical();
            FIFOWrite(left, rightInByte);
            if((rightInByte&ID_MASK) == sbID)
                FIFOWrite(up, rightInByte);
        EndCritical(status);
        rightInCount = 0;
    }

    OS_Kill();
}

////////////////////////////////////////////////////////////////////////////////
// tickTask()
// Action taken when write clock ticks. If FIFOs contain data waiting to be
//  written, writes a single byte out.
// input: none
// output: none

void tickTask(void) {
    upTick ^= 1;
    
    if(upTick) {
        if(leftOutCount > 0)
            lClkOut = 0xFF;
        if(rightOutCount > 0)
            rClkOut = 0xFF;
    }
    else {
        rClkOut = 0x00;
        lClkOut = 0x00;
        rDatOut = 0x00;
        lDatOut = 0x00;
              
        if(leftOutCount > 0) {
            leftOutCount--;
            uint8_t nextBit = (leftOutByte&(1<<leftOutCount))>>leftOutCount;
            lDatOut = 0xFF*nextBit;
        }
        
        if(leftOutCount == 0 && FIFOSize(left) > 0) {
            leftOutCount = 8;
            leftOutByte = FIFORead(left);
        }
                
        if(rightOutCount > 0) {
            rightOutCount--;
            uint8_t nextBit = (rightOutByte&(1<<rightOutCount))>>rightOutCount;
            rDatOut = 0xFF*nextBit;
        }
        
        if(rightOutCount == 0 && FIFOSize(right) > 0) {
            rightOutCount = 8;
            rightOutByte = FIFORead(right);
        }
    }

    OS_Kill();
}

////////////////////////////////////////////////////////////////////////////////
// SBInit()
// Initialized all hardware, global variables, and threads needed for smartbus
//  operation.
// input: values for write clock period, node ID#, and smartbus thread priority
// output: none

void SBInit(uint32_t inputPeriod, uint8_t inputIdentity, uint8_t inputPriority){
    sbPriority = inputPriority;
    sbID = inputIdentity;
    
    SBTimersInit(&tickTask, sbPriority, inputPeriod);
    SBPinsInit(&leftInTick, &rightInTick, sbPriority);  
}

////////////////////////////////////////////////////////////////////////////////
// SBWrite()
// Write single byte to out FIFOs.
// input: single byte to be written to smartbus
// output: none

void SBWrite(uint8_t newByte) {
    int32_t status = StartCritical();
        FIFOWrite(left, newByte);
        FIFOWrite(right, newByte);
    EndCritical(status);
}

////////////////////////////////////////////////////////////////////////////////
// SBRead()
// Read single byte from up FIFO.
// input: none
// output: returns one byte to user

uint8_t SBRead(void) {
    return FIFORead(up);
}

////////////////////////////////////////////////////////////////////////////////
// SBAvailable()
// Returns number of bytes currently held in up FIFO
// input: none
// output: current size of up FIFO

uint8_t SBAvailable(void) {
    return FIFOSize(up);
}
