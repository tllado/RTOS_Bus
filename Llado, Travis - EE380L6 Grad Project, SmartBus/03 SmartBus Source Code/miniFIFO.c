// miniFIFO.c
// barebones FIFO class for RTComms project
// Creates six 256-byte FIFOs for SmartBus communications system.
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "miniFIFO.h"
#include "OS.h"

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t FIFO[NUM_FIFOS][FIFO_LENGTH] = {0};
uint8_t start[NUM_FIFOS] = {0};
uint8_t size[NUM_FIFOS] = {0};

////////////////////////////////////////////////////////////////////////////////
// FIFOSize()
// Returns current size of data stored in FIFO
// input: FIFO number to be measured
// output: size of FIFO

uint8_t FIFOSize(uint8_t fifoNum) {
    return size[fifoNum];
}

////////////////////////////////////////////////////////////////////////////////
// FIFORead()
// Returns single byte from FIFO
// input: number of FIFO to be read from
// output: single byte from specified FIFO

uint8_t FIFORead(uint8_t fifoNum) {
    start[fifoNum]++;
    size[fifoNum]--;
    return FIFO[fifoNum   ][start[fifoNum] - 1];
}

////////////////////////////////////////////////////////////////////////////////
// FIFOWrite
// Writes single byte into specified FIFO
// input: number of FIFO to be written to, byte to be written
// output: none

void FIFOWrite(uint8_t fifoNum, uint8_t newByte) {
    FIFO[fifoNum][start[fifoNum]+size[fifoNum]] = newByte;
    size[fifoNum]++;
}
