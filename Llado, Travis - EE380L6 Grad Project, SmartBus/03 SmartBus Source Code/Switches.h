// Switches.h
// Travis Llado
// 2016.05.07

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define SW1		0x10                      // on the left side of the Launchpad board
#define SW2 	0x01                      // on the right side of the Launchpad board

////////////////////////////////////////////////////////////////////////////////
// switchesInit()
// Initialized hardware and software associated with Launchpad's two onboard
//  switches
// Input: none
// Output: none

void switchesInit(void(*task1)(void), void(*task2)(void), uint8_t swPrio);
