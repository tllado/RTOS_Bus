// SmartBus.h
// Travis Llado
// April 11th, 2016

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define up      0
#define left    1
#define right   2

#define ID_MASK 0xF0

////////////////////////////////////////////////////////////////////////////////
// SBInit()
// Initialized all hardware, global variables, and threads needed for smartbus
//  operation.
// input: values for write clock period, node ID#, and smartbus thread priority
// output: none

void SBInit(uint32_t sbFreq, uint8_t boardID, uint8_t sbPrio);

////////////////////////////////////////////////////////////////////////////////
// SBWrite()
// Write single byte to out FIFOs.
// input: single byte to be written to smartbus
// output: none

void SBWrite(uint8_t);

////////////////////////////////////////////////////////////////////////////////
// SBRead()
// Read single byte from up FIFO.
// input: none
// output: returns one byte to user

uint8_t SBRead(void);

////////////////////////////////////////////////////////////////////////////////
// SBAvailable()
// Returns number of bytes currently held in up FIFO
// input: none
// output: current size of up FIFO

uint8_t SBAvailable(void);
